﻿using System;
using Confluent.Kafka;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;

namespace ST_KafkaProducer.Controllers
{
    [Route("api/kafka")]
    [ApiController]
    public class KafkaProducerController : ControllerBase
    {
        private readonly ProducerConfig config = new ProducerConfig
            { BootstrapServers = "localhost:9092" };
        private readonly string topic = "simpletalk_topic";
        [HttpPost]
        public IActionResult Post([FromQuery] string message)
        {
            return Created(string.Empty, SendToKafka(topic, message));
        }

        public class Employee
        {
            public string Name { get; set; }
            public int Age { get; set; }
        }

        private Object SendToKafka(string topic, string message)
        {
            using (var producer =
                new ProducerBuilder<Null, string>(config).Build())
            {
                //try
                //{
                //    return producer.ProduceAsync(topic, new Message<Null, string> { Value = message })
                //        .GetAwaiter()
                //        .GetResult();
                //}
                //catch (Exception e)
                //{
                //    Console.WriteLine($"Oops, something went wrong: {e}");
                //}

                try
                {
                    var employee = new Employee() {Age = 150, Name = "Theodor"};
                    var msgString = JsonSerializer.Serialize(employee);
                    return producer.ProduceAsync(topic, new Message<Null, string> { Value = msgString })
                        .GetAwaiter()
                        .GetResult();
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Oops, something went wrong: {e}");
                }
            }
            return null;
        }
    }
}
